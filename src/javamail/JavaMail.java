/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javamail;

import java.io.File;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;


/**
 *
 * @author ZANDUT
 */
public class JavaMail
{

    
    private static String email = "yourEmail@gmail.com";
    private static String password = "Password";
    
    public static void main(String[] args)
    {
        
        email = "mfauzan613110035@gmail.com";
        password = "fauzan613110035";
        // TODO code application logic here
        
        //TLS port 587; SSL port 465
        //Properties untuk kebutuhan session
        Properties prop = new Properties();
        prop.put("mail.smtp.auth", "true");
        prop.put("mail.smtp.host", "smtp.gmail.com");
        
        //SSL
        prop.put("mail.smtp.ssl.enable", "true");
        prop.put("mail.smtp.port", "465");
        
        
        //TLS
//        prop.put("mail.smtp.starttls.enable", "true");
//        prop.put("mail.smtp.port", "587");
       
        
        Session session = Session.getDefaultInstance(prop, new Authenticator(){

            @Override
            protected PasswordAuthentication getPasswordAuthentication()
            {
                return new PasswordAuthentication(email, password);
            }
            
        });
        
        try
        {
            //instansiasi objek message dari session yang kita buat dengan username dan password
            Message message = new MimeMessage(session);
            File f = new File("C:\\Users\\ZANDUT\\Downloads\\Studi Kasus Fuzzy Systems.docx");
            DataSource data = new FileDataSource(f);
            BodyPart bodyPart = new MimeBodyPart();
            bodyPart.setDataHandler(new DataHandler(data));
            bodyPart.setFileName(f.getName());
            
            Multipart multiPart = new MimeMultipart();
            multiPart.addBodyPart(bodyPart);
            message.setContent(multiPart);
            
            //set Deliver
            message.setFrom(new InternetAddress(email));
            
            //set Recipient. RecipientType = TO/BCC/CC
            message.setRecipient(Message.RecipientType.TO, new InternetAddress(email));
            
            //set subject
            message.setSubject("Testing Subject");
            
            //set isi message
            message.setText("Test dari Netbeans");
            
            //Method send message
            Transport.send(message);
            System.out.println("Message Sent !!!");
            
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        
    }
    
}
